export default {
    // alerts
    alerts: {
        colors: {
            // default variant
            default: {
                primary: 'bg-blue-500 text-white shadow-sm border-t-8 border-blue-400',
                error: 'bg-red-500 text-white shadow-sm border-t-8 border-red-400',
                success: 'bg-yellow-500 text-white shadow-sm border-t-8 border-yellow-400',
                default: 'bg-white shadow-sm border-t-8'
            }
        }
    },
    // button
    button: {
        colors: {
            // default variant
            default: {
                primary: 'bg-blue-600 hover:bg-blue-700 focus:ring-blue-500 text-white border-transparent shadow-sm',
                secondary: 'bg-gray-600 hover:bg-gray-700 focus:ring-gray-500 border-transparent shadow-sm text-white',
                info: 'bg-blue-400 hover:bg-blue-500 focus:ring-blue-400 text-white border-transparent shadow-sm',
                warning: 'bg-yellow-600 hover:bg-yellow-700 focus:ring-yellow-500 text-white border-transparent shadow-sm',
                success: 'bg-green-600 hover:bg-green-700 focus:ring-green-500 text-white border-transparent shadow-sm',
                error: 'bg-red-600 hover:bg-red-700 focus:ring-red-500 text-white border-transparent shadow-sm',
                default: 'bg-white hover:bg-gray-50 focus:ring-gray-500 text-gray-700 border-gray-300 shadow-sm'
            },
            // link variant
            link: {
                primary: 'text-blue-600',
                secondary: 'text-gray-600',
                info: 'text-blue-600',
                warning: 'text-yellow-600',
                success: 'text-green-600',
                error: 'text-red-600',
                default: 'text-gray-500'
            },
            // outlined variant
            outlined: {
                primary: 'border-blue-600 hover:border-blue-700 text-blue-600 focus:ring-blue-500 shadow-sm',
                secondary: 'border-gray-600 hover:border-gray-700 text-gray-600 focus:ring-gray-500 shadow-sm text-gray-600',
                info: 'border-blue-400 hover:border-blue-400 text-blue-400 focus:ring-blue-300 shadow-sm',
                warning: 'border-yellow-600 hover:border-yellow-700 text-yellow-600 focus:ring-yellow-500 shadow-sm text-yellow-600',
                success: 'border-green-600 hover:border-green-700 text-green-600 focus:ring-green-500 shadow-sm text-green-600',
                error: 'border-red-600 hover:border-red-700 text-red-600 focus:ring-red-500 shadow-sm',
                default: 'border-gray-500 hover:border-gray-600 text-gray-500 focus:ring-gray-500 shadow-sm'
            }
        }
    },
    // input
    input: {
        colors: {
            default: 'focus:ring-blue-500 focus:border-blue-500'
        }
    },
    // pagination
    pagination: {
        colors: {
            default: 'text-gray-700',
            active: 'text-blue-600'
        }
    },
    // input
    textarea: {
        colors: {
            default: 'focus:ring-blue-500 focus:border-blue-500'
        }
    },
    // input
    select: {
        colors: {
            default: 'focus:ring-blue-500 focus:border-blue-500'
        }
    },
    switch: {
        colors: {
            primary: 'bg-blue-600',
            secondary: 'bg-gray-600',
            warning: 'bg-yellow-600',
            success: 'bg-green-600',
            error: 'bg-red-600',
        }
    },
}

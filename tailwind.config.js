module.exports = {
  purge: {
    content: ['./index.html', './app.config.js', './src/**/*.{vue,js,ts,jsx,tsx}'],
    options: {
      safelist: [/^rounded-/]
    }
  },
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {},
  },
  variants: {
    extend: {},
  },
  plugins: [
    require('@tailwindcss/forms'),
  ],
}

import CmsAuthLayout from '@frontend/gits-ui/src/layouts/Auth.vue'
import CmsDefaultLayout from '@frontend/gits-ui/src/layouts/Default.vue'
import CmsLayout from '@frontend/gits-ui/src/layouts/Base.vue'
import DefaultLayout from '../layouts/Default.vue'
import Home from '../views/Home.vue'
import Forms from '../components/Forms.vue'
import Tables from '../components/Table.vue'
import Vue2 from '../views/demos/vue2.vue'
import Vue3State from '../views/demos/vue3-state.vue'
import Vue3Refs from '../views/demos/vue3-refs.vue'
import Vue3Setup from '../views/demos/vue3-setup.vue'
import Vue3Composable from '../views/demos/vue3-composable.vue'
import Vue3CSS from '../views/demos/vue3-css-variables.vue'
import NotFound from '../views/errors/NotFound.vue'
import Demos from '../views/demos/index.vue'
import SignInForm from '../components/SignInForm.vue'
import Login from '../views/cms/Auth/Login.vue'
import ForgotPassword from '../views/cms/Auth/ForgotPassword.vue'
import CmsDashboard from '../views/cms/Dashboard.vue'
import UsersIndex from '../views/cms/users/UsersIndex.vue'
import UsersDetail from '../views/cms/users/UsersDetail.vue'
import UsersForm from '../views/cms/users/UsersForm.vue'
import MultipleVModels from '../views/demos/MultipleVModels.vue'
import Reactive from '../views/demos/Reactive.vue'

export default [
    {
        path: '/',
        component: DefaultLayout,
        children: [
            {path: '/', component: Home},
            {path: '/login', component: SignInForm},
            {path: '/forms', component: Forms},
            {path: '/tables', component: Tables},
            // Demos App
            {path: '/demos', component: Demos},
            {path: '/demos/vue2', component: Vue2},
            {path: '/demos/vue3-state', component: Vue3State},
            {path: '/demos/vue3-refs', component: Vue3Refs},
            {path: '/demos/vue3-setup', component: Vue3Setup},
            {path: '/demos/vue3-composable', component: Vue3Composable},
            {path: '/demos/vue3-css-vars', component: Vue3CSS},
            {path: '/demos/multiple-v-models', component: MultipleVModels},
            {path: '/demos/reactive', component: Reactive},
        ],
    },
    // CMS App
    {
        path: '/cms',
        component: CmsLayout,
        children: [
            {
                path: 'auth',
                component: CmsAuthLayout,
                children: [
                    {
                        path: 'login',
                        component: Login
                    },
                    {
                        path: 'forgot-password',
                        component: ForgotPassword
                    }
                ]
            },
            {
                path: '',
                component: CmsDefaultLayout,
                children: [
                    {
                        path: '',
                        component: CmsDashboard
                    },
                    {
                        path: 'users',
                        component: UsersIndex
                    },
                    {
                        path: 'users/create',
                        component: UsersForm
                    },
                    {
                        path: 'users/:id/edit',
                        component: UsersForm
                    },
                    {
                        path: 'users/:id',
                        component: UsersDetail
                    },
                ]
            }
        ]
    },
    {
        path: '/:pathMatch(.*)*',
        component: NotFound
    }
]

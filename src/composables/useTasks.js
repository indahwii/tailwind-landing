import {computed, ref} from "vue";

export default function useTasks() {
    const tasks = ref([
        {
            title: 'Fix bug apollo d2c',
            isCompleted: false
        },
        {
            title: 'sharing session',
            isCompleted: false
        }
    ])
    const newTask = ref('')
    const newTaskInput = ref(null)

    const addNewTask = () => {
        if (!newTask.value) {
            newTaskInput.value.focus()
            return
        }

        tasks.value.push({
            title: newTask.value,
            isCompleted: false
        })
        newTask.value = ''
    }

    const removeTask = index => {
        tasks.value.splice(index, 1)
    }

    const itemsLeft = computed(() => tasks.value.filter(item => !item.isCompleted).length)

    return {
        tasks,
        newTask,
        newTaskInput,
        addNewTask,
        removeTask,
        itemsLeft
    }
}

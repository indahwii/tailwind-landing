import {computed, onMounted, reactive, ref} from "vue";
import {useRoute, useRouter} from "vue-router";

export function getUsers(params = {}) {
    const url = new URL('https://jsonplaceholder.typicode.com/users')
    url.search = new URLSearchParams(params).toString()

    return fetch(url.toString(), {
        method: 'GET',
        headers: {
            'Content-type': 'application/json; charset=UTF-8',
        },
    })
}

export function createUserAPI(data = {}) {
    return fetch('https://jsonplaceholder.typicode.com/users', {
        method: 'POST',
        body: JSON.stringify(data),
        headers: {
            'Content-type': 'application/json; charset=UTF-8',
        },
    })
}

export function updateUserAPI(id, data = {}) {
    return fetch('https://jsonplaceholder.typicode.com/users/' + id, {
        method: 'POST',
        body: JSON.stringify(data),
        headers: {
            'Content-type': 'application/json; charset=UTF-8',
        },
    })
}

export function getUserByIDAPI(id) {
    return fetch('https://jsonplaceholder.typicode.com/users/' + id, {
        method: 'GET',
        headers: {
            'Content-type': 'application/json; charset=UTF-8',
        },
    })
}

export default function useUserAPI() {
    const route = useRoute()
    const router = useRouter()
    const form = reactive({
        name: "",
        email: "",
        phone: "",
        username: "",
        website: ""
    })

    const isLoading = ref(false)
    const id = computed(() => route.params.id)
    const isEdit = computed(() => !!id.value)
    const title = computed(() => isEdit.value ? 'Edit' : 'Add New')

    const clearForm = () => {
        form.name = ''
        form.email = ''
        form.phone = ''
        form.username = ''
        form.website = ''
    }

    const updateUser = async () => {
        isLoading.value = true
        const res = await updateUserAPI(id.value, form)
        const body = await res.json()
        isLoading.value = false

        if (res.ok) {
            return body
        }

        return false
    }

    const createUser = async () => {
        isLoading.value = true
        const res = await createUserAPI(form)
        const body = await res.json()
        isLoading.value = false

        if (res.ok) {
            clearForm()
            return body
        }

        return false
    }

    const onSuccess = () => {
        router.push('/cms/users')
    }

    const onSubmit = async () => {
        if (isEdit.value) {
            await updateUser()
            onSuccess()
        } else {
            await createUser()
            onSuccess()
        }
    }

    async function fetchDetail() {
        const res = await getUserByIDAPI(id.value)
        const body = await res.json()

        if (res.ok) {
            form.name = body.name
            form.username = body.username
            form.email = body.email
            form.phone = body.phone
            form.website = body.website
            form.phone = body.phone
        }
    }

    onMounted(() => {
        if (id.value) {
            fetchDetail()
        }
    })

    return {
        form,
        id,
        isEdit,
        title,
        onSubmit,
        createUser,
        updateUser,
        isLoading
    }
}

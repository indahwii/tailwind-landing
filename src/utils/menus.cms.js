const ComponentIcon = `<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor">
<path d="M13 7H7v6h6V7z" />
<path fill-rule="evenodd" d="M7 2a1 1 0 012 0v1h2V2a1 1 0 112 0v1h2a2 2 0 012 2v2h1a1 1 0 110 2h-1v2h1a1 1 0 110 2h-1v2a2 2 0 01-2 2h-2v1a1 1 0 11-2 0v-1H9v1a1 1 0 11-2 0v-1H5a2 2 0 01-2-2v-2H2a1 1 0 110-2h1V9H2a1 1 0 010-2h1V5a2 2 0 012-2h2V2zM5 5h10v10H5V5z" clip-rule="evenodd" />
</svg>`;

export const menus = [
    {
        title: 'Dashboard',
        to: '/cms',
        icon: `
<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor">
  <path d="M10.707 2.293a1 1 0 00-1.414 0l-7 7a1 1 0 001.414 1.414L4 10.414V17a1 1 0 001 1h2a1 1 0 001-1v-2a1 1 0 011-1h2a1 1 0 011 1v2a1 1 0 001 1h2a1 1 0 001-1v-6.586l.293.293a1 1 0 001.414-1.414l-7-7z" />
</svg>`
    },
    {
        title: 'CRUD Example',
        children: [
            {
                title: 'User',
                to: '/cms/users',
                icon: `<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor">
  <path d="M9 6a3 3 0 11-6 0 3 3 0 016 0zM17 6a3 3 0 11-6 0 3 3 0 016 0zM12.93 17c.046-.327.07-.66.07-1a6.97 6.97 0 00-1.5-4.33A5 5 0 0119 16v1h-6.07zM6 11a5 5 0 015 5v1H1v-1a5 5 0 015-5z" />
</svg>`
            },
            {
                title: 'Roles',
                to: '/cms/roles',
                icon: `<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor">
  <path fill-rule="evenodd" d="M2.166 4.999A11.954 11.954 0 0010 1.944 11.954 11.954 0 0017.834 5c.11.65.166 1.32.166 2.001 0 5.225-3.34 9.67-8 11.317C5.34 16.67 2 12.225 2 7c0-.682.057-1.35.166-2.001zm11.541 3.708a1 1 0 00-1.414-1.414L9 10.586 7.707 9.293a1 1 0 00-1.414 1.414l2 2a1 1 0 001.414 0l4-4z" clip-rule="evenodd" />
</svg>`
            },
        ]
    },
    // {
    //     title: 'Components',
    //     children: [
    //         {
    //             title: 'Alerts',
    //             to: '/cms/components/alerts',
    //             icon: ComponentIcon
    //         },
    //         {
    //             title: 'Breadcrumbs',
    //             to: '/cms/components/breadcrumbs',
    //             icon: ComponentIcon
    //         },
    //         {
    //             title: 'Buttons',
    //             to: '/cms/components/buttons',
    //             icon: ComponentIcon
    //         },
    //         {
    //             title: 'Data Table',
    //             to: '/cms/components/tables',
    //             icon: ComponentIcon
    //         },
    //         {
    //             title: 'Input',
    //             to: '/cms/components/inputs',
    //             icon: ComponentIcon
    //         },
    //         {
    //             title: 'Modal',
    //             to: '/cms/components/modals',
    //             icon: ComponentIcon
    //         },
    //         {
    //             title: 'Pagination',
    //             to: '/cms/components/paginations',
    //             icon: ComponentIcon
    //         },
    //         {
    //             title: 'Select',
    //             to: '/cms/components/selects',
    //             icon: ComponentIcon
    //         },
    //         {
    //             title: 'Textarea',
    //             to: '/cms/components/textareas',
    //             icon: ComponentIcon
    //         },
    //         {
    //             title: 'Dropdown',
    //             to: '/cms/components/dropdowns',
    //             icon: ComponentIcon
    //         },
    //         {
    //             title: 'Switch',
    //             to: '/cms/components/v-switch',
    //             icon: ComponentIcon
    //         },
    //     ]
    // }
]
